package threads.practice.ex6;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CronometruGUI extends JFrame implements ActionListener {

    private JButton start,stop,reset;
    private JPanel control, display;
    static private JTextField displayF;
    private CronometruThread CR = new CronometruThread();
    private boolean hasStarted = false;

    public CronometruGUI(){
        setLayout(null);
        setTitle("Cronometru");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initialize();
        setSize(300,200);
        setResizable(false);
        setVisible(true);
    }

    private void initialize() {
        setLayout(null);
        control = new JPanel();
        control.setBorder(BorderFactory.createTitledBorder("Control"));
        control.setLocation(10,90);
        control.setSize(265,60);
        control.setLayout(null);
        control.setVisible(true);
        add(control);

        start = new JButton("Start");
        stop = new JButton("Stop");
        reset = new JButton("Reset");

        start.setBounds(10,25,75,20);
        stop.setBounds(90,25,75,20);
        reset.setBounds(170,25,80,20);

        start.addActionListener(this);
        stop.addActionListener(this);
        reset.addActionListener(this);

        control.add(start);
        control.add(stop);
        control.add(reset);

        display = new JPanel();
        display.setBorder(BorderFactory.createTitledBorder("Cronometer:"));
        display.setLocation(10,10);
        display.setSize(265,80);
        display.setLayout(null);
        display.setVisible(true);

        displayF = new JTextField();
        displayF.setFont(new Font("Courier", Font.BOLD,20));
        displayF.setEnabled(false);
        displayF.setBounds(10,20,245,45);

        display.add(displayF);
        add(display);
    }

    public static void writeTime(int msec, int sec, int min, int ora){
        displayF.setText(ora+":"+min+":"+sec+":"+msec);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(((JButton)e.getSource()).getText().equals("Start")){
            CR.setRunning(true);
            if(hasStarted==false) {
                CR.start();
                hasStarted = true;
            }
            else CR.resume();
        }
        if(((JButton)e.getSource()).getText().equals("Stop")) {
            CR.setRunning(false);
            CR.suspend();
        }
        if(((JButton)e.getSource()).getText().equals("Reset")){
            CR.reset();
        }
    }
}

class CronometruThread extends Thread{

    private static int msec = 0,
            sec = 0,
            min = 0,
            ora = 0;
    private boolean running = false;

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void reset(){
        msec = 0;
        sec = 0;
        min = 0;
        ora = 0;
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            msec++;
            if (msec == 1000) {
                msec = 0;
                sec++;
            }
            if (sec == 60) {
                sec = 0;
                min++;
            }
            if (min == 60) {
                min = 0;
                ora++;
            }
            CronometruGUI.writeTime(msec, sec, min, ora);
        }
    }
}
class TestCronometru{
    public static void main(String[] args){
        new CronometruGUI();
    }
}
