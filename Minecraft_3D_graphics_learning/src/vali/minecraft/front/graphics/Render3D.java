package vali.minecraft.front.graphics;

import vali.minecraft.front.Game;

public class Render3D extends Render {

    public double[] zBuffer;
    private double renderDistance = 1500000;

    public Render3D(int width, int height) {
        super(width, height);
        zBuffer = new double[width * height];
    }

    public void earth(Game game) {

        double rot = game.controls.rot;
        double cosine = Math.cos(rot);
        double sine = Math.sin(rot);
        double directionZ = game.controls.xAxis;
        double directionX = game.controls.zAxis;
        double up = game.controls.yAxis;

        double headP = 20; //player head pos relative to earh
        double skyP = 2000; //sky pos relative to earth

        for (int y = 0; y < height; y++) {
            double sky = (y - height / 2.0) / height;

            double z = headP+up/ sky; //height of the player
            if (sky < 0) {
                z = skyP-up/ -sky; //height of the sky
            }

            for (int x = 0; x < width; x++) {
                double depth = (x - width / 2.0) / height;
                depth *= z;
                double x2 = depth * cosine + z * sine + directionZ;
                double y2 = z * cosine - depth * sine + directionX;
                int xPix = (int) (x2 + directionZ);
                int yPix = (int) (y2 + directionX);

                pixel[x + y * width] = 255*Texture.floor.pixel[(xPix&15)+(yPix&15)*16]/255;


                zBuffer[x + y * width] = z;

                if (z > 1000) {
                    pixel[x + y * width] = (129 << 16) | (172 << 8) | 255;
                }

            }
        }
    }

    public void rDistanceLimit() {
        //fog
        for (int i = 0; i < width * height; i++) {
            int color = pixel[i];
            int absBr = (int) (renderDistance / (zBuffer[i]));

            if (absBr < 0) {
                absBr = 0;
            }

            if (absBr > 255) {
                absBr = 255;
            }

            int r = (color >> 16) & 255, g = (color >> 8) & 255, b = (color) & 255;

            r = r * absBr/255;
            g = g * absBr/255;
            b = b * absBr/255;


            pixel[i] = (r << 16) | (g << 8) | b;
        }
    }
}
