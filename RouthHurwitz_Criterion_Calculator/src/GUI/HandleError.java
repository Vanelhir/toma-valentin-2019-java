package GUI;

import sound.Sound;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class HandleError extends JFrame implements ActionListener {

    JLabel message;
    JButton Cancel,OK;
    JLabel error;

    public HandleError(int errCode){
        action(errCode);
    }

    private void action(int errCode) {
        try {
            Sound s = new Sound("res/error1.wav");
            s.play();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        setTitle("Error");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setLayout(null);

        error = new JLabel();
        error.setIcon(new ImageIcon("res/error.png"));
        error.setBounds(10,25,80,80 );
        add(error);

        Cancel = new JButton("Cancel");
        Cancel.setBounds(100,100,80,40);

        OK = new JButton("OK");
        OK.setBounds(200,100,80,40);

        Cancel.addActionListener(this);
        OK.addActionListener(this);

        add(OK);
        add(Cancel);

        setSize(400,200);
        setResizable(false);
        setVisible(true);

        if(errCode==1) {
            //#1 error: Train station name is null
            Cancel.setEnabled(false);
            message = new JLabel("Nu ati introdus coeficientii!!!!!!!!!");
            CalculatorGUI.logContentSetter("\n>> Error: Nu ati introdus coeficientii !!");
        }

        message.setBounds(100,50,300,20);
        add(message);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(((JButton)e.getSource()).getText().equals("OK")) dispose();
        if(((JButton) e.getSource()).getText().equals("Cancel")) System.exit(1);
    }
}
