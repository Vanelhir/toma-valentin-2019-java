package vali.minecraft.input;

public class Controls {

    public double xAxis, zAxis, yAxis, rot, xRot, zRot, yRot, rotA;
    public static boolean tLeft = false;
    public static boolean tRight = false;


    public void tick(boolean forward, boolean back, boolean left, boolean right,boolean jump) {
        double rotVel = 0.05;
        double walkVel = 1;
        double xMove = 0;
        double zMove = 0;
        double jumpH = 0.5;

        if (forward) {
            zMove++;
        }
        if (back) {
            zMove--;
        }
        if (left) {
            xMove--;
        }
        if (right) {
            xMove++;
        }
        if (tLeft) {
            rotA -= rotVel;
        }
        if (tRight) {
            rotA += rotVel;
        }

        if(jump){
            yAxis += jumpH;
        }

        xRot += (xMove * Math.cos(rot) + zMove * Math.sin(rot)) * walkVel;
        zRot += (zMove * Math.cos(rot) + zMove * Math.sin(rot)) * walkVel;

        xAxis += xRot;
        yAxis *=0.9;
        zAxis += zRot;
        xRot *= 0.1;
        zRot *= 0.1;
        rot += rotA;
        rotA *= 0.5;
    }
}
