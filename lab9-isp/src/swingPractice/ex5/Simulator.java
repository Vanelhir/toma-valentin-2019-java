package swingPractice.ex5;

import java.util.*;

class Controler{

    String stationName;

    ArrayList<Controler> neighbourControllerList = new ArrayList<Controler>();

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v){
        neighbourControllerList.add(v);
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    public ArrayList<Segment> getListOfSegments() {
        return list;
    }

    public ArrayList<Controler> getNeighbourControllerList(){
        return neighbourControllerList;
    }

    boolean isNeighbour(Controler c) {
        for (int i = 0; i < neighbourControllerList.size(); i++) {
            if(neighbourControllerList.get(i).equals(c)) return true;
        }
        return false;
    }

    void removeNeighbourController(Controler v){
        for (int i = 0; i< neighbourControllerList.size();i++){
            if(v.stationName.equals(neighbourControllerList.get(i).stationName)){
                neighbourControllerList.remove(i);
                return;
            }
        }
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep() {
        //check which train must be sent
        for (int i = 0; i < neighbourControllerList.size(); i++) {
            for (Segment segment : list) {
                if (segment.hasTrain()) {
                    Train t = segment.getTrain();

                    if (t.getDestination().equals(neighbourControllerList.get(i).stationName)) {
                        //check if there is a free segment
                        int id = neighbourControllerList.get(i).getFreeSegmentId();
                        if (id == -1) {
                            SimulatorGUI.logContentSetter("Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + neighbourControllerList.get(i).stationName + ". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        SimulatorGUI.logContentSetter("Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + neighbourControllerList.get(i).stationName);
                        segment.departTRain();
                        neighbourControllerList.get(i).arriveTrain(t, id);
                    }

                }
            }//.for

        }//.
    }

    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){
            //search id segment and add train on it
            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    SimulatorGUI.logContentSetter("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
                    return;
                }else{
                    SimulatorGUI.logContentSetter("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        SimulatorGUI.logContentSetter("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");

    }


    public String displayStationState(){
        String string = new String();
        string += "=== STATION "+stationName+" ===\n";
        for(Segment s:list){
            if(s.hasTrain())
                string+="|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|\n";
            else
                string+="|----------ID="+s.id+"__Train=______ catre ________----------|\n";
        }
        return string;
    }

    @Override
    public String toString(){
        return this.stationName;
    }
}


class Segment{
    int id;
    Train train;

    Segment(int id){
        this.id = id;
    }

    boolean hasTrain(){
        return train!=null;
    }

    Train departTRain(){
        Train tmp = train;
        this.train = null;
        return tmp;
    }

    void arriveTrain(Train t){
        train = t;
    }

    Train getTrain(){
        return train;
    }
}

class Train{
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

}