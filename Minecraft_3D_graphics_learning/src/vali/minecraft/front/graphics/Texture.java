package vali.minecraft.front.graphics;

import javax.imageio.ImageIO;
import javax.xml.soap.Text;
import java.awt.image.BufferedImage;

public class Texture {
    public static Render floor = loadBitmap("/textures/grass.png");

    public static Render loadBitmap(String fileName){
        try{
            BufferedImage image = ImageIO.read(Texture.class.getResourceAsStream(fileName));
            int width = image.getWidth();
            int height = image.getHeight();
            Render res = new Render(width,height);
            image.getRGB(0,0,width,height,res.pixel,0,width);
            return res;
        }catch (Exception e){
            System.out.println("RuntimeException: could not load texture file:"+fileName);
            throw new RuntimeException(e);
        }
    }
}
