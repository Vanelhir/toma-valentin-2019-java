package isp.lab1.prob3;

import isp.lab1.prob2.Author;

public class Book extends Author {

    private static String name;
    private double price;
    private Author author;
    private int qtyInStock = 0;

    public Book(String name, String email, char gender, String name1, double price, Author author) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.author = author;
    }

    public Book(String name, String email, char gender, String name1, double price, Author author, int qtyInStock) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.author = author;
        this.qtyInStock = qtyInStock;
    }


    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString(){
        System.out.println("book-"+getName()+" by "+super.toString());
        return null;
    }
}
