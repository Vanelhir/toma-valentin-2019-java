package vali.minecraft.front;

import vali.minecraft.front.graphics.Render;
import vali.minecraft.front.graphics.Screen;
import vali.minecraft.input.Controls;
import vali.minecraft.input.inputHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;


public class Display extends Canvas implements Runnable {

    public static final int WIDTH = 854;
    public static final int HEIGHT = 480;
    public static final String TITLE = "Minecraft Rip-off";

    private Thread thread;
    private boolean running = false;
    private Screen screen;
    private BufferedImage img;
    private int[] pixels;
    private inputHandler input;

    private int newX = 0;
    private int newY = 0;
    private int oldX = 0;
    private int oldY = 0;

    private Render render;
    private Game game;
    private int fps = 0;


    public Display() {
        Dimension size = new Dimension(WIDTH, HEIGHT);
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        screen = new Screen(WIDTH, HEIGHT);
        game = new Game();
        img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt) img.getRaster().getDataBuffer()).getData();

        input = new inputHandler();
        addKeyListener(input);
        addFocusListener(input);
        addMouseListener(input);
        addMouseMotionListener(input);
    }

    public static void main(String[] args) {
        BufferedImage cursor = new BufferedImage(16,16,BufferedImage.TYPE_4BYTE_ABGR);
        Cursor cur = Toolkit.getDefaultToolkit().createCustomCursor(cursor, new Point(0,0),"No cursor");



        Display game = new Display();
        JFrame frame = new JFrame();
        frame.add(game);
        frame.pack();
        frame.getContentPane().setCursor(cur);
        frame.setTitle(TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);


        game.start();
    }

    private void start() {
        if (running) return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    private void stop() {
        if (!running) return;
        running = false;
        try {
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void run() {
        int frames = 0;
        double uSec = 0;
        double secPerTick = 1 / 60.0;
        int countTick = 0;
        boolean tickDone = false;
        long prevTime = System.nanoTime();

        while (running) {

            long currTime = System.nanoTime();
            long pasTime = currTime - prevTime;
            prevTime = currTime;
            uSec += pasTime / 1000000000.0;

            while (uSec > secPerTick) {
                tick();
                uSec -= secPerTick;
                tickDone = true;
                countTick++;
                if (countTick % 60 == 0) {
                    System.out.println(frames + " fps");
                    fps = frames;
                    frames = 0;
                }
            }
            if (tickDone) {
                render();
                frames++;
            }
            render();
            frames++;


            newX = inputHandler.MouseX;
            newY = inputHandler.MouseY;

            if (newX > oldX) {
                //right
                Controls.tRight= true;
            } else if (newX < oldX) {
                //left
                Controls.tLeft = true;
            } else {
                //still
                Controls.tLeft = false;
                Controls.tRight = false;
            }

            oldX = newX;
        }
    }

    private void tick() {
        game.tick(input.keyID);
    }

    private void render() {

        BufferStrategy bs = this.getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }


        screen.render(game);

        for (int i = 0; i < WIDTH * HEIGHT; i++) {
            pixels[i] = screen.pixel[i];
        }

        Graphics g = bs.getDrawGraphics();
        g.drawImage(img, 0, 0, WIDTH + 10, HEIGHT + 10, null);
        g.setFont(new Font("Arial", 0, 20));
        g.setColor(Color.green);
        g.drawString("FPS:" + fps, 1, 20);
        g.dispose();
        bs.show();
    }
}
