package matrix;

import GUI.CalculatorGUI;
import matrix.interfaces.MatrixHandler;

public class Matrix implements MatrixHandler {

    public Matrix() {
    }

    @Override
    public void afisareMatrice(String[][] matrice, int r, int c) {
        String row = new String();
        String line = "                |";

        int max = matrice[0][0].length();
        for(int i = 0; i< r; i ++){
            for(int j =0; j<c; j++){
                if(matrice[i][j].length()>max) max =matrice[i][j].length();
            }
        }


        for (int i = 0; i < c; i++)
            line+="_ ";

        row = "";
        for (int i = 0; i < r; i++) {
            if(i == 3) CalculatorGUI.logContentSetter(line);
            if (row != "") {
                CalculatorGUI.logContentSetter("        s^"+String.valueOf(r-i-1)+" |"+row);
                row = "";
            }
            for (int j = 0; j < c; j++)
                row += " " + matrice[i][j];
        }
        CalculatorGUI.logContentSetter("");
    }

    @Override
    public int[] setareMatrice(int numarCoef) {
        //numarCoef = coef.length
        int rand;
        int colo;
        rand = numarCoef + 1;
        if (numarCoef % 2 == 0) colo = numarCoef / 2;
        else colo = numarCoef / 2 + 1;

        int[] param = new int[2];
        param[0]=rand;
        param[1]=colo;

        return param;
    }

    @Override
    public String[][] creareMatrice(int rand, int colo){
        //creerea matricii routh-hurwitz
        String[][] routhHurwitz = new String[rand][colo];
        for (int i = 0; i < rand; i++)
            for (int j = 0; j < colo; j++)
                routhHurwitz[i][j] = "0";

        return routhHurwitz;
    }
}
