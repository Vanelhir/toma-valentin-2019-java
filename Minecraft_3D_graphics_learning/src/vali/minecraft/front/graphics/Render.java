package vali.minecraft.front.graphics;

import vali.minecraft.front.Display;

public class Render {
    public final int width;
    public final int height;
    public final int[] pixel;

    public Render(int width, int height) {
        this.width = width;
        this.height = height;
        pixel = new int[width * height];
    }

    public void draw(Render render, int xOffset, int yOffset) {
        for (int y = 0; y < render.height; y++) {
            int yPixel = y + yOffset;
            if (yPixel < 0 || yPixel >= height) continue;
            for (int x = 0; x < render.width; x++) {
                int xPixel = x + xOffset;
                if (xPixel < 0 || xPixel >= width) continue;

                int alpha = render.pixel[x + y * render.width];
                if (alpha > 0) {
                    pixel[xPixel + yPixel * width] = render.pixel[x + y * render.width];
                }
            }
        }
    }
}
