package org.practice.game;

import java.awt.*;
import java.lang.String;
import java.awt.image.BufferStrategy;
import java.awt.Graphics;
import java.awt.Color;


public class Game extends Canvas implements Runnable{

    private static final long serialVersionUID = -240000000000000L;
    private Thread thread;
    private boolean running = false;

    private Handler handler;
    private HUD hud;
    public static int frames;
    public static long timer = System.currentTimeMillis();
    public static final int WIDTH = 768, HEIGHT = 768;



    public Game(){
        handler = new Handler();
        this.addKeyListener(new KeyInput(handler));

        new Window(WIDTH, HEIGHT,"Snake", this);

        hud = new HUD();
        handler.addObject(new Player(WIDTH/2-32,HEIGHT/2-32,ID.Player, handler));
        handler.addObject(new Player2(WIDTH/2+128,HEIGHT/2+128,ID.Player2, handler));
    }

    public static int cLamp(int x, int i, int width) {
        if(x >= width) return x = width;
        else if(x <= i) return x = i;
        else return x;
    }

    public synchronized  void start(){
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    public synchronized  void stop(){
        try{
            thread.join();
            running = false;
        }catch(Exception exceptie){
            exceptie.printStackTrace();
        }
    }

    public void run(){
        //game loop, de pe net :P
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;

        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime)/ns;
            lastTime = now;
            while(delta >= 1){
                tick();
                delta--;
            }
            if(running)
                render();
            frames++;

            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
                System.out.println("FPS: "+frames);
                frames=0;
            }

        }
        stop();
    }

    private void tick(){
        handler.tick();
        hud.tick();
    }

    private void render(){
        BufferStrategy SB = this.getBufferStrategy();
        if(SB == null){
            this.createBufferStrategy(3);
            return;
        }

        Graphics grafica = SB.getDrawGraphics();
        grafica.setColor(Color.black);
        grafica.fillRect(0,0,WIDTH,HEIGHT);

        handler.render(grafica);
        hud.render(grafica);

        grafica.dispose();
        SB.show();
    }

    public static void main(String args[]){
        new Game();
    }
}
