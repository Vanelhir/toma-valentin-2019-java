package matrix.interfaces;

import java.util.Vector;

public interface MatrixHandler{
    public void afisareMatrice(String[][] matrice, int r, int c);
    public int[] setareMatrice(int numarCoef);
    public String[][] creareMatrice(int rand, int colo);
}
