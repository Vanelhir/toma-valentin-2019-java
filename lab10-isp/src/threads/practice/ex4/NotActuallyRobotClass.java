package threads.practice.ex4;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class NotActuallyRobotClass extends Thread {

    String nume;
    int PosX;
    int PosY;
    Random r =new Random();

    public NotActuallyRobotClass(String nume, int PosX, int PosY) {
        super(nume);
        this.nume = nume;
        this.PosX = PosX;
        this.PosY = PosY;
    }

    public void run() {
        while (true) {

            PosX = r.nextInt();
            PosY = r.nextInt();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(nume+"Position:"+PosX+"."+PosY);
            Plansa.checkCollision();
            }
        }
    }


class TestRobots{

    static public NotActuallyRobotClass Robots[] = new NotActuallyRobotClass[10];

    public static void main(String[] args){
        Robots[0] = new NotActuallyRobotClass("Robot1",0,0); 
        Robots[1] = new NotActuallyRobotClass("Robot2",0,0);
        Robots[2] = new NotActuallyRobotClass("Robot3",0,0);
        Robots[3] = new NotActuallyRobotClass("Robot4",0,0);
        Robots[4] = new NotActuallyRobotClass("Robot5",0,0);
        Robots[5] = new NotActuallyRobotClass("Robot6",0,0);
        Robots[6] = new NotActuallyRobotClass("Robot7",0,0);
        Robots[7] = new NotActuallyRobotClass("Robot8",0,0);
        Robots[8] = new NotActuallyRobotClass("Robot9",0,0);
        Robots[9] = new NotActuallyRobotClass("Robot10",0,0);
        Plansa p = new Plansa();
        Robots[0].start();
        Robots[1].start();
        Robots[2].start();
        Robots[3].start();
        Robots[4].start();
        Robots[5].start();
        Robots[6].start();
        Robots[7].start();
        Robots[8].start();
        Robots[9].start();
        }
    }


class Plansa{

    int dimX = 100;
    int dimY = 100;
    static int maxDim = TestRobots.Robots.length;
    static ArrayList<NotActuallyRobotClass> FlaggedRobots = new ArrayList<NotActuallyRobotClass>();
    static int current = 0;

    public static void checkCollision(){
        boolean ok = true;
        for(int i = 0; i< maxDim; i++) {
            for (int j = 0; j < maxDim; j++) {
                if (i != j) {
                    if ((TestRobots.Robots[i].PosX == TestRobots.Robots[j].PosX) && (TestRobots.Robots[i].PosY == TestRobots.Robots[j].PosY)) {
                        for (NotActuallyRobotClass currentRobot : FlaggedRobots) {
                            if (TestRobots.Robots[i].equals(currentRobot)) ok = false;
                            if (TestRobots.Robots[j].equals(currentRobot)) ok = false;
                        }

                        if (ok == true) {
                            System.out.println(TestRobots.Robots[i].nume + " collided with " + TestRobots.Robots[j].nume + " and both exploded.");
                            TestRobots.Robots[i].stop();
                            TestRobots.Robots[j].stop();
                            FlaggedRobots.add(TestRobots.Robots[i]);
                            FlaggedRobots.add(TestRobots.Robots[j]);
                        }
                    }
                }
            }
        }
    }
}