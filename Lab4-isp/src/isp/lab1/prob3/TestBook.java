//package isp.lab1.prob3;
//
//import isp.lab1.prob2.Author;
//
//public class TestBook {
//    public static void main(String[] args) {
//        Author A = new Author("Catalin", "ctlbudecan@gmail.com", 'm');
//        Book B1 = new Book("Budecan","catalinbudecan@gmail.com",'m',"The Book",A,350);
//        Book B2 = new Book("Ciortea","cristiciortea@gmail.com",'m',"Cars",A,450,5);
//        System.out.println("The author of B1 is "+ B1.getAuthor());
//        System.out.println("The price of the book 'The Book' is "+ B1.getPrice());
//        B1.setPrice(555);
//        System.out.println("The new price of the book 'The Book' is "+ B1.getPrice());
//        System.out.println("The quantity of the book 'Cars' is "+ B2.getQtyInStock());
//    }
//}