package vali.minecraft.front;

import vali.minecraft.input.Controls;

import java.awt.event.KeyEvent;

public class Game {
    public int time;
    public Controls controls;

    public Game(){
        controls = new Controls();
    }

    public void tick(boolean[] keyID){
        time++;
        boolean fwd = keyID[KeyEvent.VK_W];
        boolean bcw = keyID[KeyEvent.VK_S];
        boolean lft = keyID[KeyEvent.VK_A];
        boolean rgt = keyID[KeyEvent.VK_D];
        boolean jump = keyID[KeyEvent.VK_SPACE];


        controls.tick(fwd,bcw,lft,rgt,jump);
    }
}
