package GUI;

import routh_hurwitz.RouthHurwitz;

import javax.print.attribute.standard.Media;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class SetCoefGUI extends JFrame implements ActionListener {

    JLabel message;
    JButton Cancel,OK;
    JTextField input;
    RouthHurwitz routhHurwitzHandler = new RouthHurwitz();

     public SetCoefGUI(){
        setTitle("Seteaza Coeficienti");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        init();
    }

    private void init() {


         Cancel = new JButton("Cancel");
        Cancel.setBounds(100,100,80,40);

        OK = new JButton("OK");
        OK.setBounds(200,100,80,40);

        Cancel.addActionListener(this);
        OK.addActionListener(this);

        add(OK);
        add(Cancel);

        setSize(400,200);
        setResizable(false);
        setVisible(true);

        message = new JLabel("Introdu coeficientii separati prin spatiu +k:");
        message.setBounds(10,20,300,20);
        add(message);

        input = new JTextField();
        input.setBounds(10,50,365,30);
        add(input);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(((JButton)e.getSource()).getText().equals("OK")){
            String[] coef = routhHurwitzHandler.citireCoef(input.getText());
            String message = "";
            for (String co:coef) {
                message+=co+" ";
            }
            CalculatorGUI.logContentSetter("\n>> Coeficienti setati:\n\n\n"+"       "+message+"\n");
            CalculatorGUI.setCoef(coef);
            dispose();
        }
        if(((JButton) e.getSource()).getText().equals("Cancel")) dispose();
    }
}
