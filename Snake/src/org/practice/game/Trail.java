package org.practice.game;

import java.awt.*;

public class Trail extends GameObject{

    private float a = 1;
    private Handler handler;
    private Color color;
    private int width, height;
    private float life;

    public Trail(int x, int y, Color color, int width, int height, float life, ID id, Handler handler){
        super(x,y,id);
        this.handler = handler;
        this.color = color;
        this.width = width;
        this.height = height;
        this.life = life;
    }

    public void tick(){
        if(a>life){
            a -= life - 0.001f;
        }
        else handler.removeObject(this);
    }

    public void render(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g2d.setComposite(makeTransparent(a));
        g.setColor(color);
        g.fillRect(x,y,width,height);
        g2d.setComposite(makeTransparent(1));
    }

    public Rectangle getBounds() {
        return new Rectangle(x,y, 32, 32);
    }

    private AlphaComposite makeTransparent(float alpha){
        int type = AlphaComposite.SRC_OVER;
        return(AlphaComposite.getInstance(type,alpha));
    }
}
