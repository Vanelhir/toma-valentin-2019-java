package org.practice.game;

import java.awt.*;

public class Player2 extends GameObject {

    Handler handler;
    int rand1 = (int) (Math.random() * 768);
    int rand2 = (int) (Math.random() * 768);

    public Player2(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler = handler;
    }


    public Rectangle getBounds() {
        return new Rectangle(x, y, 16, 16);
    }

    public void tick() {
        x += velX;
        y += velY;

        x = Game.cLamp(x, 0, Game.WIDTH - 48);
        y = Game.cLamp(y, 0, Game.HEIGHT - 70);

        collision();
        handler.addObject(new Trail2(x, y, Color.blue, 16, 16, 0.01f, ID.Trail, handler));
    }

    private void collision() {

        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);


            if (tempObject.getId() == ID.Food || tempObject.getId() == ID.Player) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    HUD.SCORE++;

                    handler.removeObject(tempObject);
                    rand1 = (int) (Math.random() * 720);
                    rand2 = (int) (Math.random() * 720);
                    handler.addObject(new Food(rand1, rand2, ID.Food));

                }
                else if(getBounds().intersects(tempObject.getBounds())){
                    HUD.SCORE++;
                }
            }
        }
    }

    public void render(Graphics grafica) {

        Graphics2D g2d = (Graphics2D) grafica;

        grafica.setColor(Color.blue);
        grafica.fillRect(x, y, 16, 16);

    }
}
