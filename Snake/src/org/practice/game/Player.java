package org.practice.game;

import java.awt.*;
import java.util.Random;

public class Player extends GameObject{

    Handler handler;
    int rand1 = (int)(Math.random() * 768);
    int rand2 = (int)(Math.random() * 768);


    public Player(int x, int y, ID id, Handler handler){
        super(x,y,id);
        this.handler = handler;
        handler.addObject(new Food(rand1,rand2,ID.Food));
    }


    public Rectangle getBounds() {
        return new Rectangle(x,y, 16, 16);
    }

    public void tick(){
        x += velX;
        y += velY;

        x = Game.cLamp(x,0,Game.WIDTH-480);
        y = Game.cLamp(y,0,Game.HEIGHT-70);

        collision();
        handler.addObject(new Trail(x,y,Color.green,16,16,0.01f, ID.Trail, handler));
    }

    private void collision() {
        if (handler.object.size() == 1) handler.addObject(new Food(rand1, rand2, ID.Food));

        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);


            if (tempObject.getId() == ID.Food || tempObject.getId() == ID.Player2) {
                if (getBounds().intersects(tempObject.getBounds())) {
                    HUD.SCORE++;

                    handler.removeObject(tempObject);
                    rand1 = (int) (Math.random() * 700);
                    rand2 = (int) (Math.random() * 700);
                    handler.addObject(new Food(rand1, rand2, ID.Food));

                }
                else if(getBounds().intersects(tempObject.getBounds())){
                    HUD.SCORE++;
                }
            }
        }
    }

    public void render(Graphics grafica){

        Graphics2D g2d = (Graphics2D) grafica;


        grafica.setColor(Color.green);
        grafica.fillRect(x, y, 16, 16);

    }
}
