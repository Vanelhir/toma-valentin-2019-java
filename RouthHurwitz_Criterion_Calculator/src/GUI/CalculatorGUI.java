package GUI;

import matrix.Matrix;
import routh_hurwitz.RouthHurwitz;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorGUI extends JFrame implements Runnable, ActionListener {

    static JTextArea logContent;
    JButton calculate;
    JButton setCoef;
    JButton reset;
    JButton exit;
    JLabel univLogo;
    static String logText = ">> Routh-Hurwitz: K-critical calculator (ver. 1.1.beta).\n";
    Matrix matrice = new Matrix();
    RouthHurwitz calculRouthHurwitz = new RouthHurwitz();
    static String[] coef = null;

    public CalculatorGUI(){
        setTitle("Routh-Hurwitz calculator.");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(1280,720);
        setResizable(false);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        univLogo = new JLabel();
        univLogo.setIcon(new ImageIcon("res/logoutcn.jpg"));
        univLogo.setBounds(1150,0,150,150);
        add(univLogo);

        logContent = new JTextArea(20,20);
        JScrollPane scroll = new JScrollPane(logContent);
        scroll.setBounds(10,30,1130,640);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        add(scroll);

        exit = new JButton("Exit");
        exit.setBounds(1150,630,105,40);
        add(exit);

        reset = new JButton("Reset");
        reset.setBounds(1150,580,105,40);
        add(reset);

        calculate = new JButton("Calculeaza");
        calculate.setBounds(1150,530,105,40);
        add(calculate);

        setCoef = new JButton("Set Coef.");
        setCoef.setBounds(1150,480,105,40);
        add(setCoef);

        exit.addActionListener(this);
        reset.addActionListener(this);
        calculate.addActionListener(this);
        setCoef.addActionListener(this);
    }

    @Override
    public void run() {

        int[] param = matrice.setareMatrice(coef.length);

        int rand = param[0];
        int colo = param[1];
        String[][] routhHurwitz;

        routhHurwitz = matrice.creareMatrice(rand, colo);


        logContentSetter(">> Matricea Routh-Hurwitz inainte de calculare:\n\n");
        routhHurwitz = calculRouthHurwitz.fillRouthHurwitz(routhHurwitz,coef);
        matrice.afisareMatrice(routhHurwitz,rand,colo);

        logContentSetter(">> Matricea Routh-Hurwitz dupa calculare:\n\n");
        routhHurwitz = calculRouthHurwitz.computeRouthHurwitz(routhHurwitz,rand,colo);
        matrice.afisareMatrice(routhHurwitz,rand,colo);

        boolean semn = calculRouthHurwitz.determinaSemn(routhHurwitz,rand);
        double k = calculRouthHurwitz.findKCritic(routhHurwitz,rand,colo);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(((JButton)e.getSource()).getText().equals("Exit")) dispose();
        if(((JButton)e.getSource()).getText().equals("Reset")) {
            coef = null;
            logContentSetter(">> Coeficientii au fost resetati!");
        }
        if(((JButton)e.getSource()).getText().equals("Calculeaza")) {
            if(checkCoef(coef)) new HandleError(1);
            else {
                run();
            }
        }
        if(((JButton)e.getSource()).getText().equals("Set Coef.")){
            new SetCoefGUI();
        }
    }

    public static void setCoef(String[] coeficienti) {
        coef = coeficienti;
    }

    public static void logContentSetter(String input) {
        logText = logText + "\n" + input;
        logContent.setText(logText);
    }

    public static boolean checkCoef(String[] coef){
        if(coef==null) return true;
        else return false;
    }

    public static void main(String[] args) {
        new CalculatorGUI();
        CalculatorGUI.logContentSetter("");
    }
}
