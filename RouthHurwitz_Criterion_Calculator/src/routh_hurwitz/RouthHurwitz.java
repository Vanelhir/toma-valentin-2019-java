package routh_hurwitz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import GUI.CalculatorGUI;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.Function;
import routh_hurwitz.interfaces.RouthHurwitzHandler;

public class RouthHurwitz implements RouthHurwitzHandler {

    public String[] citireCoef(String line){
        //BufferedReader br = new BufferedReader(new InputStreamReader());
        //String line = null;
//        try {
//            line = br.readLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        String[] coef = line.trim().split("\\s+");
        return coef;
    }

    public String[][] fillRouthHurwitz(String[][] routhHurwitz, String[] coef){
        int counterRand = 0;
        int counterCol = 0;
        for (int i = 0; i < coef.length; i++) {
            routhHurwitz[counterRand][counterCol] = coef[i];
            counterRand++;
            if (counterRand == 2) {
                counterRand = 0;
                counterCol++;
            }
        }
        return routhHurwitz;
    }

    public String[][] computeRouthHurwitz(String[][] routhHurwitz,int rand, int colo){
        Expression e;
        int curent;
        int colNew = colo;
        int randPrimA = 0, randPrimB = 1;
        for (int i = 2; i < rand - 1; i++) {
            curent = 1;
            for (int j = 0; j < colNew; j++) {
                routhHurwitz[i][j] = "((" + routhHurwitz[randPrimB][0] + "*" + routhHurwitz[i - 2][curent] + "-" + routhHurwitz[randPrimA][0] + "*" + routhHurwitz[i - 1][curent] + ")/" + routhHurwitz[i - 1][0] + ")";
                if (!routhHurwitz[i][j].contains("k")) {
                    e = new Expression(routhHurwitz[i][j]);
                    routhHurwitz[i][j] = String.valueOf(e.calculate());
                }
                curent++;
                if (curent >= colNew) {
                    colNew--;
                    break;
                }
            }
            randPrimA++;
            randPrimB++;
        }
        return routhHurwitz;
    }

    public boolean determinaSemn(String[][] routhHurwitz,int rand){
        boolean stabil = true;
        boolean semn = true; // true = plus, false = minus;
        if (Double.valueOf(routhHurwitz[0][0]) < 0) semn = false;

        for (int i = 0; i < rand; i++) {
            if (semn)
                if (!routhHurwitz[i][0].contains("k")) {
                    if (Double.valueOf(routhHurwitz[i][0]) < 0) stabil = false;
                } else if (!routhHurwitz[i][0].contains("k")) {
                    if (Double.valueOf(routhHurwitz[i][0]) > 0) stabil = false;
                }
        }
        CalculatorGUI.logContentSetter(">> Sistemul este stabil:" + stabil+"\n");
        return semn;
    }

    public Double findKCritic(String[][] routhHurwitz, int rand, int colo){
        //aflarea lui K critic
         ArrayList<Double> kResultList = new ArrayList<Double>();
         ArrayList<Expression> expressions = new ArrayList<Expression>();
         ArrayList<Expression> equations = new ArrayList<Expression>();
         ArrayList<Double> finalResults = new ArrayList<Double>();

        for (int i = 0; i < rand; i++) {
            if (routhHurwitz[i][0].contains("k")) {
                expressions.add(new Expression("(" + routhHurwitz[i][0] + ")"));
            }
        }

        for (Expression exp : expressions) {
            equations.add(new Expression("solve(" + exp.getExpressionString() + ",k,0,99999999999999)"));
        }

        for (Expression eq : equations) {
            kResultList.add(eq.calculate());
        }

        for (Double k : kResultList) {
            boolean solutie = true;
            for (Expression exp : expressions) {
                Function f = new Function("f", exp.getExpressionString(), "k");
                if (f.calculate(k) < 0 || k == 0 || Double.isNaN(k)) {
                    solutie = false;
                }
            }
            if (solutie) finalResults.add(k);
        }
        Object obj;
        if (finalResults.size() > 0) {
            obj = Collections.min(finalResults);
            CalculatorGUI.logContentSetter(">> Sistemul este stabil pt K < " + obj+"\n");
            return Double.valueOf(obj.toString());
        }
        else {
            CalculatorGUI.logContentSetter(">> Sistemul este stabil pt orice K > 0.\n");
            return 0.0;
        }
    }


}
