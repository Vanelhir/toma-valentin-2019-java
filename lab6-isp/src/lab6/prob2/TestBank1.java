package lab6.prob2;

import lab6.prob1.BankAccount;

import java.util.Collections;
import java.util.Comparator;

public class TestBank1 {
        public static void main(String[] args) {
            Bank1 bank = new Bank1();
            bank.addAccount("Popescu",2000);
            bank.addAccount("Ionescu",1000);
            bank.addAccount("Porceasca,",3000);
            bank.addAccount("Plescescu",20);

            bank.printAccounts();

            //bank.printAccounts(30,2000);

            //new TestBank1().printAllAccounts(bank);
        }


        void printAllAccounts(Bank1 bank){

            Collections.sort(bank.getAllAccounts(), new Comparator<BankAccount>(){
                public int compare(BankAccount a1, BankAccount a2){
                    return String.valueOf(a1.getOwner()).compareTo(a2.getOwner());
                }
            });

            for(int i = 0; i<bank.getAllAccounts().size(); i++ ){
                System.out.println("Owner's name:"+bank.getAllAccounts().get(i).getOwner()+" with a balance of:"+bank.getAllAccounts().get(i).getBalance());
            }
        }
}
