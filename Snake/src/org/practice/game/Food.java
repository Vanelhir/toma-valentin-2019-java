package org.practice.game;

import java.awt.*;

public class Food extends GameObject{
    public Food(int x, int y, ID id){
        super(x,y,id);
    }

    public void tick(){
        x += velX;
        y += velY;
    }

    public void render(Graphics grafica){
        if(id == ID.Food) {
            grafica.setColor(Color.red);
            grafica.fillOval(x, y, 32, 32);
        }
    }

    public Rectangle getBounds() {
        return new Rectangle(x,y, 32, 32);
    }
}
