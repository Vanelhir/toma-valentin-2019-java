package routh_hurwitz.interfaces;

public interface RouthHurwitzHandler {

    public String[] citireCoef(String line);

    public String[][] computeRouthHurwitz(String[][] routhHurwitz, int rand, int colo);

    public boolean determinaSemn(String[][] routhHurwitz, int rand);

    public Double findKCritic(String[][] routhHurwitz, int rand, int colo);

}
